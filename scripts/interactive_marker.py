#!/usr/bin/python

import rospy
import tf.transformations
import numpy as np

from visualization_msgs.msg import *
from interactive_markers.interactive_marker_server import \
    InteractiveMarkerServer, InteractiveMarkerFeedback
from visualization_msgs.msg import InteractiveMarker, \
    InteractiveMarkerControl
from geometry_msgs.msg import PoseStamped
from franka_msgs.msg import FrankaState

marker_pose = PoseStamped()
initial_pose_found = False
pose_pub = None
# [[min_x, max_x], [min_y, max_y], [min_z, max_z]]
position_limits = [[-0.6, 0.6], [-0.6, 0.6], [0.03, 0.9]]

marker = Marker()

marker.type = Marker.CUBE
marker.scale.x = 0.05
marker.scale.y = 0.05
marker.scale.z = 0.05
marker.color.r = 0.5
marker.color.g = 0.5
marker.color.b = 0.5
marker.color.a = 1.0


def publisherCallback(msg, link_name):
    marker_pose.header.frame_id = link_name
    marker_pose.header.stamp = rospy.Time(0)
    pose_pub.publish(marker_pose)
    marker1_pub.publish( marker1 )
    marker2_pub.publish( marker2 )


def franka_state_callback(msg):
    marker_pose.pose.orientation.x = msg.pose.orientation.x
    marker_pose.pose.orientation.y = msg.pose.orientation.y
    marker_pose.pose.orientation.z = msg.pose.orientation.z
    marker_pose.pose.orientation.w = msg.pose.orientation.w
    marker_pose.pose.position.x = msg.pose.position.x
    marker_pose.pose.position.y = msg.pose.position.y
    marker_pose.pose.position.z = msg.pose.position.z
    global initial_pose_found
    initial_pose_found = True


def processFeedback(feedback):
    if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        marker_pose.pose.position.x = max([min([feedback.pose.position.x,
                                          position_limits[0][1]]),
                                          position_limits[0][0]])
        marker_pose.pose.position.y = max([min([feedback.pose.position.y,
                                          position_limits[1][1]]),
                                          position_limits[1][0]])
        marker_pose.pose.position.z = max([min([feedback.pose.position.z,
                                          position_limits[2][1]]),
                                          position_limits[2][0]])
        marker_pose.pose.orientation = feedback.pose.orientation
    server.applyChanges()


if __name__ == "__main__":
    rospy.init_node("equilibrium_pose_node")
    state_sub = rospy.Subscriber("panda_mover/panda_pose",
                                 PoseStamped, franka_state_callback)
    listener = tf.TransformListener()
    link_name = rospy.get_param("~link_name")

    # Get initial pose for the interactive marker
    while not initial_pose_found:
        rospy.sleep(1)
    state_sub.unregister()
    
    marker1_pub = rospy.Publisher("marker1",Marker, queue_size=10)
    marker2_pub = rospy.Publisher("marker2",Marker, queue_size=10)
        
    marker1 = Marker()
    marker2 = Marker()

    marker1.header.frame_id = "panda_link0"
    marker1.header.stamp = rospy.Time(0)
    marker1.type = Marker.CUBE
    marker1.pose.position.x = 0.53;
    marker1.pose.position.y = -0.36 + 0.11
    marker1.pose.position.z = 0.025
    marker1.scale.x = 0.05
    marker1.scale.y = 0.05
    marker1.scale.z = 0.05
    marker1.color.r = 0.0
    marker1.color.g = 0.0
    marker1.color.b = 1
    marker1.color.a = 1.0


    marker2.header.frame_id = "panda_link0"
    marker2.header.stamp = rospy.Time(0)
    marker2.type = Marker.CUBE
    marker2.pose.position.x = 0.8 -0.27
    marker2.pose.position.y = -0.36 + 0.65
    marker2.pose.position.z = 0.025
    marker2.scale.x = 0.05
    marker2.scale.y = 0.05
    marker2.scale.z = 0.05
    marker2.color.r = 0.0
    marker2.color.g = 0.0
    marker2.color.b = 1
    marker2.color.a = 1.0
    marker2_pub.publish( marker2 )
        
    pose_pub = rospy.Publisher(
        "desired_pose", PoseStamped, queue_size=10)
    server = InteractiveMarkerServer("equilibrium_pose_marker")
    int_marker = InteractiveMarker()
    int_marker.header.frame_id = link_name
    int_marker.scale = 0.3
    int_marker.name = "Desired_pose"
    int_marker.pose = marker_pose.pose
    # run pose publisher
    rospy.Timer(rospy.Duration(0.001),
                lambda msg: publisherCallback(msg, link_name))



    # insert a box
    control =  InteractiveMarkerControl()
    control.always_visible = True
    control.markers.append( marker )
    control.name = "cube"
    control.interaction_mode = InteractiveMarkerControl.MOVE_ROTATE_3D
    int_marker.controls.append( control )
    
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = "rotate_x"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)

    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = "move_x"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = "rotate_y"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = "move_y"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = "rotate_z"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = "move_z"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    server.insert(int_marker, processFeedback)

    server.applyChanges()

    rospy.spin()


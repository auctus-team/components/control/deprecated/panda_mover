cmake_minimum_required(VERSION 2.8.3)
project(panda_mover)

list (APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

set(CMAKE_BUILD_TYPE Release)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(catkin REQUIRED COMPONENTS
  gazebo_ros
  controller_interface
  geometry_msgs
  realtime_tools
  dynamic_reconfigure
  franka_hw
  franka_control
  franka_gripper
  hardware_interface
  pluginlib
  roscpp
  rospy  
  trac_ik_lib
  qpOASES
  kdl_conversions
  eigen_conversions
  std_msgs
  panda_traj
  sensor_msgs
)

find_package(Franka 0.5.0 REQUIRED)
find_package(gazebo REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(Boost REQUIRED COMPONENTS date_time)
find_package(orocos_kdl REQUIRED)

generate_dynamic_reconfigure_options(
  cfg/param.cfg
)

add_message_files(
  FILES
  PandaRunMsg.msg
  qpMsg.msg
)

add_service_files(
  FILES
  UI.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs
  geometry_msgs
  sensor_msgs
)

add_library(${PROJECT_NAME} 
  src/controller/controller.cpp
  src/controller/chainjnttojacdotsolver.cpp
  src/robot/panda_control.cpp
  src/robot/panda_simulation.cpp
)

add_dependencies(${PROJECT_NAME}
  ${${PROJECT_NAME}_EXPORTED_TARGETS}
  ${catkin_EXPORTED_TARGETS}
  ${PROJECT_NAME}_gencfg
)

target_link_libraries(${PROJECT_NAME}
  ${Franka_LIBRARIES}
  ${catkin_LIBRARIES}
  ${Boost_LIBRARIES}  
  ${orocos_kdl_LIBRARIES}
  ${GAZEBO_LIBRARIES}
)

target_include_directories(panda_mover SYSTEM PUBLIC
  include
  ${GAZEBO_INCLUDE_DIRS}
  ${Franka_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

target_include_directories(panda_mover PUBLIC
  include/
  src
  ${GAZEBO_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

catkin_package(
  LIBRARIES panda_mover 
  INCLUDE_DIRS include  
  CATKIN_DEPENDS
    controller_interface
    dynamic_reconfigure
    franka_hw
    franka_control
    franka_gripper
    geometry_msgs
    nav_msgs
    hardware_interface
    message_runtime
    pluginlib
    realtime_tools
    roscpp
    actionlib_msgs
    actionlib
    std_msgs
    trac_ik_lib
    panda_traj
    sensor_msgs
  DEPENDS 
    Franka
    roscpp
    Boost
    orocos_kdl
)


## Installation

install(TARGETS panda_mover
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp"
)

install(DIRECTORY launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(DIRECTORY config
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(FILES panda_mover_plugin.xml
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(FILES scripts/killgazebo
    PERMISSIONS OWNER_READ GROUP_READ WORLD_READ WORLD_EXECUTE GROUP_EXECUTE OWNER_EXECUTE
    DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Tools
include(${CMAKE_CURRENT_LIST_DIR}/../cmake/ClangTools.cmake OPTIONAL
  RESULT_VARIABLE CLANG_TOOLS
)

if(CLANG_TOOLS)
  file(GLOB_RECURSE SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp)
  file(GLOB_RECURSE HEADERS
    ${CMAKE_CURRENT_SOURCE_DIR}/include/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.h
  )
  add_format_target(panda_mover FILES ${SOURCES} ${HEADERS})
  add_tidy_target(panda_mover
    FILES ${SOURCES}
    DEPENDS panda_mover
  ) 
  
endif()

## Build robotbody_diff tool
add_executable(robotbody_diff src/util/robotbody_diff.cpp)
target_include_directories(robotbody_diff PUBLIC
  include/
  ${catkin_INCLUDE_DIRS}
  ${CMAKE_CURRENT_BINARY_DIR}
)
target_link_libraries(robotbody_diff ${catkin_LIBRARIES} ${orocos_kdl_LIBRARIES})
install(TARGETS robotbody_diff RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#pragma once

#include <memory>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono> 

// Messages
#include <realtime_tools/realtime_publisher.h>
#include <panda_mover/PandaRunMsg.h>
#include <kdl_conversions/kdl_msg.h>
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64MultiArray.h>

// Trajectory generation
#include "panda_traj/panda_traj.hpp"
#include <panda_traj/PublishTraj.h>
#include <panda_traj/TrajProperties.h>

// UI
#include <panda_mover/UI.h>

// Controller
#include <controller_interface/controller_base.h>
#include <controller_interface/multi_interface_controller.h>
// #include <dynamic_reconfigure/server.h>
#include <geometry_msgs/PoseStamped.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <ros/ros.h>
#include <ros/node_handle.h>
#include <ros/time.h>
#include <Eigen/Dense>
#include <qpOASES.hpp>
#include <boost/scoped_ptr.hpp>
#include <trac_ik/trac_ik.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chain.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/chaindynparam.hpp>
#include <panda_mover/controller/chainjnttojacdotsolver.hpp>

// // Dynamic reconfigure
// #include <panda_mover/paramConfig.h>

// Gripper 
#include <actionlib/client/simple_action_client.h>

// Franka Panda headers
#include <franka/robot.h>
#include <franka_hw/franka_model_interface.h>
#include <franka_hw/franka_state_interface.h>
#include <franka_control/services.h>
#include <franka_gripper/GraspAction.h>  // Note: "Action" is appended
#include <franka_gripper/HomingAction.h> // Note: "Action" is appended
#include <franka_gripper/MoveAction.h>   // Note: "Action" is appended
#include <franka_gripper/StopAction.h>   // Note: "Action" is appended

#define TIME_DT 0.001
#define HORIZON_DT 0.015


namespace Controller {
    class Controller{
    public:
            bool init(ros::NodeHandle& node_handle, Eigen::VectorXd q_init, Eigen::VectorXd qd_init);
            Eigen::VectorXd update(Eigen::VectorXd q, Eigen::VectorXd qd, const ros::Duration& period, std::string &control_level);

    private:
        
        template<class T>
        bool getRosParam(const std::string& param_name, T& param_data, bool verbose=true)
        {
            if(!ros::param::get(param_name,param_data))
            {
                ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
                ros::shutdown();
            }
            else
                if (verbose) ROS_INFO_STREAM(param_name << " : " << param_data);
            return true;
        }
    
        bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose=true)
        {
            std::vector<double> std_param;
            if(!ros::param::get(param_name,std_param))
            {
                ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
                ros::shutdown();
            }
            else
            {
                if(std_param.size() == param_data.size())
                {
                    for(int i = 0; i < param_data.size() ; ++i )
                        param_data(i) = std_param[i];
                    if (verbose) ROS_INFO_STREAM(param_name << " : [" << param_data.transpose() << "]");
                    return true;
                }
                else
                {
                    ROS_FATAL_STREAM("Wrong matrix size for param " << param_name << ". Check the Yaml config file. Killing ros" );
                    ros::shutdown();
                }
            }
        }
        
        /**
        * @brief Initializes publishers
        */
        void init_publishers(ros::NodeHandle& node_handle);
        
        /**
        * @brief Loads parameters from yaml file
        */
        void load_parameters();
        
        /**
        * @brief Loads Panda robot
        */
        bool load_robot(ros::NodeHandle& node_handle);
        
        /**
        * @brief Publishes values
        */
        void do_publishing();
        
        /**
        * @brief Compute max joint velocities at next time step for stopping
        */
        void max_joint_velocities(double &t_c, double &t_acq);

        /**
        * @brief Defines a desired pose from Rviz Interactive Marker
        */
        void DesiredPoseCallback(const geometry_msgs::PoseStampedConstPtr& msg);

        /**
        * @brief Solve Joint velocity problem
        */
        Eigen::VectorXd joint_velocity_solver(Eigen::VectorXd &q, Eigen::VectorXd &qd);

        /**
        * @brief Solve Joint torque problem
        */
        Eigen::VectorXd joint_torque_solver(Eigen::VectorXd &q, Eigen::VectorXd &qd);

        /**
        * @brief Get the next trajectory frame
        */
        void increment_trajectory();

        /**
        * @brief Builds the trajectory
        */
        void build_trajectory(KDL::Frame X_curr_);

        /**
        * @brief Publishes the trajectory
        */
        void publish_trajectory();
        
        /**
        * @brief Close gripper action
        */
        void gripper_close();
        
        /**
        * @brief Open gripper action
        */
        void gripper_open();

        /**
        * @brief Update UI
        */
        bool updateUI(panda_mover::UI::Request& req, panda_mover::UI::Response& resp);

        // Convert a KDL rotation to an Eigen rotation matrix.
        Eigen::Matrix3d toEigenMatrix(KDL::Rotation const & input) {
            return (Eigen::Matrix3d{} <<
                input(0, 0),  input(0, 1), input(0, 2),
                input(1, 0),  input(1, 1), input(1, 2),
                input(2, 0),  input(2, 1), input(2, 2)
            ).finished();
        }

        // Convert a Eigen rotation to KDL rotation matrix.
        KDL::Rotation toKDLMatrix(Eigen::Matrix3d const & input) {
            return (KDL::Rotation(input(0, 0),  input(0, 1), input(0, 2),
                input(1, 0),  input(1, 1), input(1, 2),
                input(2, 0),  input(2, 1), input(2, 2))
            );
        }

        // Cross product
        KDL::Vector crossProduct(KDL::Vector const & V1, KDL::Vector const & V2) {
            return (KDL::Vector(V1(1)*V2(2)-V1(2)*V2(1),
                                -V1(0)*V2(2)+V1(2)*V2(0),
                                V1(0)*V2(1)-V1(1)*V2(0))
            );
        }

        // Difference between two rotation matrices computed as the difference rotation from vector v1 to v2 scaled by the angle of rotation.
        KDL::Vector diffAxes(KDL::Rotation const & R1, KDL::Rotation const & R2) {
            KDL::Vector u = crossProduct(R1.UnitZ(), R2.UnitZ());
            double angle = std::acos(dot(R1.UnitZ(), R2.UnitZ()));
            return angle * u;
        }

        // Difference between two KDL frames
        KDL::Twist diffFrame(KDL::Frame const & F1, KDL::Frame const & F2) {
            KDL::Twist diff;
            diff.vel = KDL::diff(F1.p, F2.p);
            diff.rot = diffAxes(F1.M, F2.M);
            return diff;
        }
        
        // Service
        ros::ServiceServer service;
        
        // Publishers
        geometry_msgs::Pose X_curr_msg_,X_traj_msg_,fake_human_pose_msg_,X_wireloop_msg_;
        geometry_msgs::Twist X_err_msg_,Xd_msg_,Xdd_msg_,xd_des_msg_,xdd_des_msg_,integral_error_msg_;  
        realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> pose_array_publisher;
        realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> polyline_pose_array_publisher;
        realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> polyline_inter_pose_array_publisher;
        realtime_tools::RealtimePublisher<nav_msgs::Path> path_publisher;
        realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> panda_pose_publisher;
        realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> wireloop_pose_publisher;
        realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> trajectory_pose_publisher;
        realtime_tools::RealtimePublisher<panda_mover::PandaRunMsg> panda_rundata_publisher;

        // Action clients
        actionlib::SimpleActionClient<franka_gripper::HomingAction> *franka_gripper_homing_action_client;
        actionlib::SimpleActionClient<franka_gripper::MoveAction> *franka_gripper_move_action_client;
        actionlib::SimpleActionClient<franka_gripper::StopAction> *franka_gripper_stop_action_client;
        actionlib::SimpleActionClient<franka_gripper::GraspAction> *franka_gripper_grasp_action_client;
        
        // Subscribers
        ros::Subscriber human_workspace_dist_sub, sub_desired_pose_, human_laser_workspace_dist_sub;
        
        boost::shared_ptr<TRAC_IK::TRAC_IK> ik_solver;
        boost::scoped_ptr<KDL::ChainJntToJacSolver> chainjacsolver_;
        boost::scoped_ptr<KDL::ChainJntToJacDotSolver> chainjnttojacdotsolver_;
        
        /**
        * @brief The dynamic solver.
        */
        boost::scoped_ptr<KDL::ChainDynParam> dynModelSolver_;
        
        /**
        * @brief The forward kinematic solver for position
        */
        boost::scoped_ptr<KDL::ChainFkSolverPos_recursive> fksolver_;

        /**
        * @brief The forward kinematic solver for velocity
        */
        boost::scoped_ptr<KDL::ChainFkSolverVel_recursive> fksolvervel_;
        
        KDL::Chain chain;
        KDL::JntArray ll, ul, q_kdl, qd_kdl, coriolis_kdl, gravity_kdl, tau_kdl;
        KDL::Twist Jdqd_kdl;
        KDL::Jacobian J_;
        KDL::JntSpaceInertiaMatrix M_, M_inv_;    
        KDL::Frame X_curr_, X_last_, X_init_, X_traj_,X_traj_next;
        KDL::FrameVel Xd_curr_;
        KDL::Twist Xd_traj_, Xdd_traj_, X_err_, Xd_curr_twist_;
        KDL::JntArrayVel q_in;
        Eigen::VectorXd p_gains_, d_gains_, i_gains_, p_gains_qd_;
        Eigen::VectorXd velocity_p_gains_, velocity_d_gains_, velocity_p_gains_qd_;
        Eigen::VectorXd torque_p_gains_, torque_d_gains_, torque_p_gains_qd_;
        Eigen::VectorXd torque_max_, jnt_vel_max_, damping_weight_;
        Eigen::Matrix<double,6,1> jdot_qdot_, xdd_des_, xd_des_, xd_curr_, x_curr_, x_err, xd_traj, xdd_traj, integral_error_;
        double regularisation_weight_;
        int dof;
        bool sim=false;
        bool test_constraints=false;
        ros::Time last_time_;
        std::string root_link_, tip_link_;
        double diff_angle = 0;
        double prev_diff_angle = -9999;
        double integral_pos_saturation_, integral_rot_saturation_;

        // Torque variables
        Eigen::Matrix<double,7,1> gravity,coriolis;
        Eigen::Matrix <double,6,7> J;
        Eigen::Matrix <double,7,7> M;
        Eigen::Matrix <double,6,6> RR;
        Eigen::Matrix <double,3,3> R_ee_base;
        KDL::Rotation R_ee_base_kdl;
                
        // Save variables
        std::chrono::steady_clock::time_point starttime;
        std::chrono::steady_clock::time_point endtime;
        std::chrono::duration<double> cycletime;
        int num_cycles;
        bool run_cycletime_test = false;
        bool save_this_update = false;
        std::ofstream testsavefile, runsavefile;
        std::string test_save_path_ = "~/test_data.txt";
        std::string run_save_path_ = "~/run_data.txt";

        // Matrices for qpOASES
        // NOTE: We need RowMajor (see qpoases doc)
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> H_;
        Eigen::VectorXd g_;
        Eigen::VectorXd lb_, ub_;
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> A_;
        Eigen::VectorXd lbA_, ubA_, qd_min_, qd_max_,qdd_max_, q_mean_, qd_prev_, qdd_curr_, qddd_max_;
        Eigen::VectorXd nonLinearTerms_;
        std::unique_ptr<qpOASES::SQProblem> qpoases_solver_;
        int number_of_constraints_;
        int number_of_variables;        
        bool qp_solver_failed = false;
        double max_kinetic_energy, ec_computed;
        Eigen::Matrix<double,6,Eigen::Dynamic> a_;
        Eigen::Matrix<double,6,1> b_;
        Eigen::Matrix<double,6,6> selection_matrix_;
        Eigen::VectorXd control_action, joint_velocity_out_, joint_torque_out_;
        qpOASES::Constraints constraints_;
        qpOASES::Bounds bounds_;
        Eigen::VectorXd activated_cons_,activated_bounds_;
        std_msgs::Float64MultiArray activated_bounds_msg_,activated_cons_msg_;
        bool positioning_;
        Eigen::VectorXd select_axes_;
        std::string control_level_;
        bool manual_movement = false;
        bool virtual_walls = false;
        bool joint_angles_valid = false;
        double q2_lb = 1.1;
        double q2_ub = 1.3;
        double q7_lb, q7_ub, q7_lb_manual, q7_ub_manual, q7_hold;
        double error_multiplier=1;

        // Telemanipulation
        bool telemanipulation;
        KDL::Frame X_traj_prev_,X_traj_new_;
        Eigen::Vector3d filtered_position;
        double x,y,z,w,new_x,new_y,new_z,new_w;
        double position_filter,orientation_filter;
        bool CloseGripper, OpenGripper;
        
        // Trajectory variables
        bool publish_traj,play_traj_,override_distance_;
        int traj_gripper_counter=0;
        TrajectoryGenerator trajectory;
        panda_traj::TrajProperties traj_properties_;
        std::string trajectory_file = "";
        Eigen::Matrix<double,3,1> u;
        double vmax, amax, vmax_curr;
        KDL::Vector dir_;
        double amax_traj_;   
        bool near_path = false;   
        bool must_rebuild = true;  
    };
}
#endif // CONTROLLER_HPP
